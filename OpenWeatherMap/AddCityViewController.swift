//
//  AddCityViewController.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 18/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

import UIKit
import Kingfisher

class AddCityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    var cities = [City]()
    var citiesSelected = [String]()
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    let cellReuseIdentifier = "cell"
    let db: OpaquePointer? = DatabaseManager.openDatabase()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        searchBar.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(cancelButtonAttributes, for: .normal)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        readValues(searchString: searchText)
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CityTempCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CityTempCell
        
        let city = cities[indexPath.row]
        cell.cityNameLabel.text = city.country! + " " + city.name!
        if (city.country != "") {
            cell.countryFlagImageView.kf.setImage(with: URL(string: "https://www.countryflags.io/\(city.country ?? "")/flat/64.png"))
        }
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let city = cities[indexPath.row]
        self.appendToCitiesSelectedStored(cityCode: String(city.id))
        NotificationCenter.default.post(name: Notification.Name("ReloadTable"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func readValues(searchString: String){
        cities = DatabaseManager.queryCities(database: db, searchString: searchString)
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Store Settings Methods
    
    func appendToCitiesSelectedStored (cityCode: String) {
        self.citiesSelected.append(cityCode)
        saveUpdatedCitiesSelectedStored()
    }
    func saveUpdatedCitiesSelectedStored() {
        let defaults = UserDefaults.standard
        defaults.setValue(self.citiesSelected, forKey: defaultsKeys.citiesSelectedStored)
        defaults.synchronize()
    }
    
}
