//
//  ViewController.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 17/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var editBtn: UIBarButtonItem!
    @IBOutlet weak var addBtn: UIBarButtonItem!
    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBOutlet weak var lastUpdateMessage: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    let cellReuseIdentifier = "cell"
    var citiesSelected = [String]()
    var mDataSet = [CityWeather]()
    var citiesUpdated = false
    
    var barButtonTintColorDefault: UIColor!
    
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateValuesAndRefresh), name: Notification.Name("ReloadTable"), object: nil)
    
        setNavigationBarItems()
        
        updateValuesAndRefresh()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.updateValuesAndRefresh), for: UIControl.Event.valueChanged)
        
        tableView.backgroundView = refreshControl
        
        lastUpdateMessage.layer.borderWidth = 1
        lastUpdateMessage.layer.borderColor = UIColor(red: 26/255, green: 35/255, blue: 40/255, alpha: 1.0).cgColor
        
        getCitiesSelectedStored()
    }

    func openAddCityViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addCityVC = storyboard.instantiateViewController(withIdentifier: "AddCityViewController") as! AddCityViewController
        addCityVC.citiesSelected = self.citiesSelected
        self.navigationController?.pushViewController(addCityVC, animated: true)
    }
    
    func setNavigationBarItems() {
        navItem.title = "Open Weather Map"
        editBtn.title = "Editar"
        barButtonTintColorDefault = addBtn.tintColor
        addBtn.isEnabled = false
        addBtn.tintColor = UIColor.clear
    }
    
    @objc func updateValuesAndRefresh() {
        self.hideUpdateMessage()
        
        getCitiesSelectedStored()
        
        if (self.citiesSelected.count > 0) {
            getDataAndRefresh()
        } else {
            Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(self.endRefresh), userInfo: nil, repeats: false)
            let alert = UIAlertController(title: "Querés seleccionar una ciudad?", message: "Hacelo ahora o más tarde presionando el botón Editar y luego el botón +", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
                self.changeEditTableStatus()
                self.openAddCityViewController()
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    
    func getCitiesSelectedStored() {
        let defaults = UserDefaults.standard
        self.citiesSelected = defaults.stringArray(forKey: defaultsKeys.citiesSelectedStored) ?? [String]()
    }

    func getDataAndRefresh(){
        loadLastValues()
    }
    
    func loadLastValues() {
        var url: NSURL
        var tempDataSet = [CityWeather]()
        let totalCities = self.citiesSelected.count
        var handledCitites = 0
        for cityId in self.citiesSelected {
            //creating a NSURL
            url = NSURL(string: "https://api.openweathermap.org/data/2.5/weather?id=\(cityId)&appid=\(AppConstants.openWeatherMapApiKey)")!
            
            //fetching the data from the url
            URLSession.shared.dataTask(with: (url as URL), completionHandler: {(data, response, error) -> Void in
                
                if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
                    
                    let cityName = jsonObj.value(forKey: "name")! as? String ?? ""
                    print(cityName)
                    
                    let city = City()
                    let cityWeather = CityWeather()
                    
                    city.id = Int(cityId)!
                    city.name = cityName
                    if let jsonSysObj = jsonObj.value(forKey: "sys")! as? NSDictionary {
                        city.country = jsonSysObj.value(forKey: "country")! as? String ?? ""
                    }
                    
                    cityWeather.city = city
                    if let jsonMainObj = jsonObj.value(forKey: "main")! as? NSDictionary {
                        cityWeather.temp = Utils.getCelcius(tempKelvin: jsonMainObj.value(forKey: "temp")! as? Double ?? 0.00)!
                        cityWeather.temp_min = Utils.getCelcius(tempKelvin: jsonMainObj.value(forKey: "temp_min")! as? Double ?? 0.00)!
                        cityWeather.temp_max = Utils.getCelcius(tempKelvin: jsonMainObj.value(forKey: "temp_max")! as? Double ?? 0.00)!
                        cityWeather.pressure = jsonMainObj.value(forKey: "pressure")! as? Int ?? 0
                        cityWeather.humidity = jsonMainObj.value(forKey: "humidity")! as? Int ?? 0
                    }
                    
                    if let weather = jsonObj["weather"] as? [[String: Any]] {
                        if weather.count > 0 {
                            cityWeather.weatherIcon = weather[0]["icon"] as? String ?? ""
                            cityWeather.weatherMain = weather[0]["main"] as? String ?? ""
                            cityWeather.weatherDescription = weather[0]["description"] as? String ?? ""
                        } else {
                            cityWeather.weatherIcon = ""
                            cityWeather.weatherMain = ""
                            cityWeather.weatherDescription = ""
                        }
                    }
                    
                    tempDataSet.append(cityWeather)
                }
                
                handledCitites = handledCitites + 1
                
                DispatchQueue.main.async {
                    if (handledCitites == totalCities) {
                        self.mDataSet.removeAll()
                        self.mDataSet = tempDataSet
                        self.reorderCitiesToShow()
                        self.tableView.reloadData()
                        Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(self.endRefresh), userInfo: nil, repeats: false)
                        let date = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
                        let result = formatter.string(from: date)
                        self.lastUpdateMessage.text = "Última actualización: " + result
                        self.citiesUpdated = true
                    }
                    return
                }
            }).resume()
        }
    }
    
    func reorderCitiesToShow(){
        var auxDataSet = [CityWeather]()
        for cityId in self.citiesSelected {
            for cityWeather in self.mDataSet {
                if cityId == String(cityWeather.city.id) {
                    auxDataSet.append(cityWeather)
                }
            }
        }
        self.mDataSet.removeAll()
        self.mDataSet = auxDataSet
    }
    
    @objc func endRefresh() {
        self.refreshControl.endRefreshing()
        if citiesUpdated {
            self.showUpdateMessage()
            citiesUpdated = false
        }
    }
    
    func showUpdateMessage(){
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.hideUpdateMessage), userInfo: nil, repeats: false)
        lastUpdateMessage.isHidden = false
    }
    
    @objc func hideUpdateMessage(){
        lastUpdateMessage.isHidden = true
    }
    
    
    
    @IBAction func editPressed(_ sender: UIBarButtonItem) {
        changeEditTableStatus()
    }
    @IBAction func addPressed(_ sender: UIBarButtonItem) {
        openAddCityViewController()
    }
    
    func changeEditTableStatus() {
        tableView.isEditing = !tableView.isEditing
        if (tableView.isEditing) {
            navItem.title = "Ciudades"
            editBtn.title = "Listo"
            addBtn.isEnabled = true
            addBtn.tintColor = barButtonTintColorDefault
        } else {
            navItem.title = "Open Weather Map"
            editBtn.title = "Editar"
            addBtn.isEnabled = false
            addBtn.tintColor = UIColor.clear
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mDataSet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CityTempCell
        cell.cityNameLabel.text = self.mDataSet[indexPath.row].city.name
        cell.temperatureValueLabel.text = Utils.getTemperatureFormatted(temp: self.mDataSet[indexPath.row].temp, decimals: 1)
        cell.temperatureMinMaxLabel.text = "\( Utils.getTemperatureFormatted(temp: self.mDataSet[indexPath.row].temp_min, decimals: 0)) / \( Utils.getTemperatureFormatted(temp: self.mDataSet[indexPath.row].temp_max, decimals: 0))"
        if (self.mDataSet[indexPath.row].weatherIcon != "") {
            cell.weatherStatusImageView.kf.setImage(with: URL(string: "https://openweathermap.org/img/w/\(self.mDataSet[indexPath.row].weatherIcon).png"))
        }
        if (self.mDataSet[indexPath.row].weatherMain != "") {
            cell.weatherMainLabel.text = self.mDataSet[indexPath.row].weatherMain
        }
        if (self.mDataSet[indexPath.row].weatherDescription != "") {
            cell.weatherDescriptionLabel.text = self.mDataSet[indexPath.row].weatherDescription
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("You tapped cell number \(indexPath.row).")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailsVC = storyboard.instantiateViewController(withIdentifier: "CityDetailsViewController") as! CityDetailsViewController
        detailsVC.cityWeather = self.mDataSet[indexPath.row]
        navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let selectedItem = self.mDataSet[sourceIndexPath.row]
        self.mDataSet.remove(at: sourceIndexPath.row)
        self.mDataSet.insert(selectedItem, at: destinationIndexPath.row)
        self.citiesSelected.remove(at: sourceIndexPath.row)
        self.citiesSelected.insert(String(selectedItem.city.id), at: destinationIndexPath.row)
        saveUpdatedCitiesSelectedStored()
    }
    
    internal func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.mDataSet.remove(at: indexPath.row)
            self.citiesSelected.remove(at: indexPath.row)
            saveUpdatedCitiesSelectedStored()
            tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Store Settings Methods
    
    func appendToCitiesSelectedStored (cityCode: String) {
        self.citiesSelected.append(cityCode)
        saveUpdatedCitiesSelectedStored()
    }
    func removeFromCurrenciesSelectedStored (cityCode: String) {
        let indexToRemove = self.citiesSelected.firstIndex(of: cityCode)
        if (indexToRemove != nil) {
            self.citiesSelected.remove(at: indexToRemove!)
            saveUpdatedCitiesSelectedStored()
        }
    }
    func saveUpdatedCitiesSelectedStored() {
        let defaults = UserDefaults.standard
        defaults.setValue(self.citiesSelected, forKey: defaultsKeys.citiesSelectedStored)
        defaults.synchronize()
    }
    
}
