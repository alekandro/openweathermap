//
//  CityTemp.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 18/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

class CityWeather {
    var city: City = City()
    var temp: Double = 0.00
    var pressure: Int = 0
    var humidity: Int = 0
    var temp_min: Double = 0.00
    var temp_max: Double = 0.00
    var weatherIcon: String = ""
    var weatherMain: String = ""
    var weatherDescription: String = ""
    init() {
    }
    init(city: City, temp: Double, pressure: Int, humidity: Int, temp_min: Double, temp_max: Double, mainWeather: String, mainDescription: String){
        self.city = city
        self.temp = temp
        self.pressure = pressure
        self.humidity = humidity
        self.temp_min = temp_min
        self.temp_max = temp_max
    }
}
