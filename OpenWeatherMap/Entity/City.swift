//
//  CityTemp.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 18/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

class City {
    var id: Int = 0
    var country: String? = ""
    var name: String? = ""
    init() {
    }
    init(id: Int, country: String?, name: String?){
        self.id = id
        self.country = country
        self.name = name
    }
}
