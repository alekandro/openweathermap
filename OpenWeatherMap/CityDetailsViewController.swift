//
//  CityDetailsViewController.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 19/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

import UIKit
import XJYChart
import Kingfisher

class CityDetailsViewController: UIViewController {
    var cityWeather: CityWeather = CityWeather()
    
    @IBOutlet weak var cityTemp: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var viewForChart: UIView!
    @IBOutlet weak var weatherMainLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var temperatureMaxMinLabel: UILabel!
    
    var mDataSetDay = [String]()
    var mDataSetTemp = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cityTemp?.text = Utils.getTemperatureFormatted(temp: cityWeather.temp, decimals: 1)
        cityNameLabel?.text = cityWeather.city.name
        weatherMainLabel?.text = cityWeather.weatherMain
        weatherDescriptionLabel?.text = cityWeather.weatherDescription
        temperatureMaxMinLabel?.text = "\( Utils.getTemperatureFormatted(temp: cityWeather.temp_min, decimals: 1)) / \( Utils.getTemperatureFormatted(temp: cityWeather.temp_max, decimals: 1))"
        loadNextDays()
    }
    
    func loadNextDays() {
        var url: NSURL
        self.mDataSetDay.removeAll()
        self.mDataSetTemp.removeAll()
        
        //creating a NSURL
        url = NSURL(string: "https://api.openweathermap.org/data/2.5/forecast?id=\(Int(cityWeather.city.id))&appid=\(AppConstants.openWeatherMapApiKey)")!
        
        //fetching the data from the url
        URLSession.shared.dataTask(with: (url as URL), completionHandler: {(data, response, error) -> Void in
            
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
                if let list = jsonObj["list"] as? [[String: Any]] {
                    for day in list {
                        let date = Date(timeIntervalSince1970: day["dt"] as! TimeInterval)
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd/M \n HH"
                        let result = formatter.string(from: date)
                        self.mDataSetDay.append(result)
                        if let jsonMainObj = day["main"]! as? NSDictionary {
                            self.mDataSetTemp.append(Int(Utils.getCelcius(tempKelvin: jsonMainObj.value(forKey: "temp")! as? Double ?? 0.00)!))
                        }
                    }
                }
                
            }
            DispatchQueue.main.async {
                self.loadChart()
                return
            }
            
        }).resume()
    }
    
    func loadChart() {
        var itemArray: [AnyHashable] = []
        let numberArray = self.mDataSetTemp
        let item = XLineChartItem(dataNumber: NSMutableArray(array:numberArray), color: ChartConstants.XJYWhite)
        itemArray.append(item!)
        let configuration = XAreaLineChartConfiguration()
        configuration.isShowPoint = true
        configuration.lineMode = XLineMode.CurveLine
        configuration.ordinateDenominator = 6
        //    configuration.isEnableNumberLabel = YES;
        let daysArray = self.mDataSetDay
        let lineChart = XLineChart(frame: CGRect(x: 0, y: 0, width: self.viewForChart.frame.size.width, height: 200), dataItemArray: NSMutableArray(array: itemArray), dataDiscribeArray: NSMutableArray(array:daysArray), topNumber: 40, bottomNumber: -40, graphMode: XLineGraphMode.AreaLineGraph, chartConfiguration: nil)
        viewForChart.addSubview(lineChart!)
    }
    
}
