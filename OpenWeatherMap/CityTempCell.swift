//
//  CityTempCell.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 18/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

import UIKit
class CityTempCell: UITableViewCell {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var temperatureValueLabel: UILabel!
    @IBOutlet weak var temperatureMinMaxLabel: UILabel!
    @IBOutlet weak var weatherStatusImageView: UIImageView!
    @IBOutlet weak var weatherMainLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
}
