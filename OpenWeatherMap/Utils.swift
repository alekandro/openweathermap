//
//  Utils.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 19/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

import Foundation

class Utils {
    static func getCelcius(tempKelvin: Double) -> Double? {
        return tempKelvin > 0 ? (tempKelvin - 273.15) : 0.00
    }
    static func getTemperatureFormatted(temp: Double, decimals: Int) -> String {
        return String(format: "%.\(decimals)f", ceil(temp * 100) / 100) + "º"
    }
}
