//
//  DatabaseManager.swift
//  OpenWeatherMap
//
//  Created by Alejandro Vazquez Alvarez on 19/05/2019.
//  Copyright © 2019 Telecom. All rights reserved.
//

import Foundation
import SQLite3

class DatabaseManager {
    static func openDatabase() -> OpaquePointer? {
        var db: OpaquePointer?
        // generando ruta del archivo
        let fileURL = Bundle.main.path(forResource: "cities", ofType: "sqlite")!
        
        // abriendo DB
        if sqlite3_open(fileURL, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        return db
    }
    
    static func queryCities(database: OpaquePointer?, searchString: String) -> [City] {
        var cities = [City]()
        // creando la consulta a la DB
        let queryString = "SELECT * FROM citylist WHERE name LIKE '%\(searchString)%'"
    
        // statement pointer
        var stmt:OpaquePointer?
    
        // realizando la consulta
        if sqlite3_prepare(database, queryString, -1, &stmt, nil) != SQLITE_OK{
        let errmsg = String(cString: sqlite3_errmsg(database)!)
        print("error haciendo query: \(errmsg)")
        return cities
        }
    
        // guardando registros en cities
        while(sqlite3_step(stmt) == SQLITE_ROW){
            let id = sqlite3_column_int(stmt, 0)
            let country = String(cString: sqlite3_column_text(stmt, 1))
            let name = String(cString: sqlite3_column_text(stmt, 2))
        
            cities.append(City(id: Int(id), country: String(describing: country), name: String(describing: name)))
        }
        return cities
    }
}
